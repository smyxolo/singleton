package zadania.GraLiczbowa;

import java.io.FileNotFoundException;

public class GameManager {
    public static void main(String[] args) {
        try {
            ReadConfig rc = new  ReadConfig();
            rc.MakeReadConfig();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        System.out.println("Math game. Guess the result " + MySettings.getInstance().getRoundLimit() + " times and win!");
        int wins = 0;
        for (int i = 0; i < MySettings.getInstance().getRoundLimit(); i++) {
            Game g = new Game();
            if(g.play()) wins++;
            System.out.println("End of round " + (i+1) + ". " + wins + " games won out of " + MySettings.getInstance().getRoundLimit() + " rounds");
        }
        if (wins > MySettings.getInstance().getRoundLimit() / 2) System.out.println("You've won the series!");
        else System.out.println("You've lost the series.");
        System.out.println("Score: " + wins + "/" + MySettings.getInstance().getRoundLimit() + " games.");
    }
}
