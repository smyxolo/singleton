package zadania.GraLiczbowa;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class ReadConfig {

    File file = new File("/Users/marekdybusc/IdeaProjects/Programowanie2Amen/Singleton/src/main/java/zadania/GraLiczbowa/Config.txt");
    Scanner fileScan = new Scanner(file);

    public ReadConfig() throws FileNotFoundException {
    }


    public void MakeReadConfig() throws FileNotFoundException {
        while (fileScan.hasNextLine()){
            String[] inputArgs = fileScan.nextLine().split("=");
            if(inputArgs[0].contains("zakres_liczby_1")){
                MySettings.getInstance().setNumber1Range(Integer.parseInt(inputArgs[1]));
            }
            else if (inputArgs[0].contains("zakres_liczby_2")){
                MySettings.getInstance().setNumber2Range(Integer.parseInt(inputArgs[1]));
            }
            else if (inputArgs[0].contains("ilosc_rund")){
                MySettings.getInstance().setRoundLimit(Integer.parseInt(inputArgs[1]));
            }
            else if(inputArgs[0].contains("dostepne_dzialania")){
                if(inputArgs[1].contains("+")){
                    MySettings.getInstance().getUsedOperators().add(Operators.ADD);
                }
                if(inputArgs[1].contains("-")){
                    MySettings.getInstance().getUsedOperators().add(Operators.SUB);
                }
                if(inputArgs[1].contains("*")){
                    MySettings.getInstance().getUsedOperators().add(Operators.MUL);
                }
                if(inputArgs[1].contains("/")){
                    MySettings.getInstance().getUsedOperators().add(Operators.DIV);
                }
            }
        }
    }
}
