package zadania.DziwnyZus;

public class TicketGenerator {

    int counter  = 0;
    private static final TicketGenerator INSTANCE = new TicketGenerator();

    private TicketGenerator() {}


    public static TicketGenerator getInstance(){
        return INSTANCE;
    }

    public int generateTicket(){
        return ++counter;
    }
}
