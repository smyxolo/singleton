Singleton  2
Stworzymy  praktyczny  zbiór  klas  odpowiadający  za  przechowywanie  konfiguracji wewnątrz  aplikacji.  Naturalne j est  posiadanie  w  programie  plików  konfiguracyjnych które  zawierają  pewne  parametry  wykonania.  My  stworzymy  aplikację  która  będzie ł adowała dane  z  pliku  konfiguracyjnego i  będzie  przechowywała  w  singletonie ( aby  mieć  do  nich dostęp  wszędzie).
Nasza  aplikacja  będzie  czymś  w r odzaju  gry.  Zasada  działania  gry.  Po r ozpoczęciu  gry aplikacja l osuje  dwie liczby,  oraz l osuje  działanie  matematyczne,  a  następnie  zadaje  pytanie  użytkownikowi: Ile  wynosi { liczba1} { działanie} { liczba2}?
i  użytkownik  musi  odpowiedzieć.  Przykład:
liczba  wylosowana  1 :   50 liczba  wylosowana  2 :   30 działanie  wylosowane :  *
Zadane  pytanie: Ile  wynosi  50 *   30?
Jeśli  użytkownik  odpowie  poprawnie  otrzymuje  punkt, j eśli  niepoprawnie  nie  otrzymuje punktu.
Aplikacja  ma  ustawienia ł adowane  z  pliku: zakres_liczby_1=1000 zakres_liczby_2=10000 dostepne_dzialania=*/+-
ilosc_rund=10
Na  podstawie t ych  ustawien  powinna  byc  prowadzona r ozgrywka  10 r und.  po  10 r undach zaprezentuj  uzytkownikowi  wynik.
Stwórz: -  klasę  która  odpowiada  za  czytanie  konfiguracji  z  pliku i ł adowanie  go  do  klasy " MySettings" -  klasę  MySettings  która  posiada  mapę  konfiguracji