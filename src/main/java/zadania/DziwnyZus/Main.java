package zadania.DziwnyZus;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner skan = new Scanner(System.in);
        boolean working = true;
        String inputLine = "";

        WaitingRoom wr = new WaitingRoom();
        CashRegistry cr = new CashRegistry();
        HealthDepartment hd = new HealthDepartment();

        while (!inputLine.equals("quit")){
            inputLine = skan.nextLine();
            if(inputLine.equals("quit")) break;
            if(inputLine.startsWith("waiting")){
                System.out.println(wr.generateTicket());
            }
            else if(inputLine.startsWith("health")){
                System.out.println(hd.generateTicket());
            }
            else if(inputLine.startsWith("cash")){
                System.out.println(cr.generateTicket());
            }
            else System.out.println("Wrong command");
        }
    }
}
