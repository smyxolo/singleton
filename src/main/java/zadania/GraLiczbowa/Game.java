package zadania.GraLiczbowa;

import java.util.Random;
import java.util.Scanner;

public class Game {
    private int number1;
    private int number2;
    private Random random = new Random();
    private Operators randomOperator = getRandomOperator();

    public Game() {
        this.number1 = random.nextInt(MySettings.getInstance().getNumber1Range()) + 1;
        this.number2 = random.nextInt(MySettings.getInstance().getNumber2Range()) + 1;
    }

    private Operators getRandomOperator(){
        int randomIndex = random.nextInt(MySettings.getInstance().getUsedOperators().size());
        return MySettings.getInstance().getUsedOperators().get(randomIndex);
    }

    private int getResult(){
        switch (randomOperator){
            case ADD: {
                return number1 + number2;
            }case SUB: {
                return number1 - number2;
            }case MUL: {
                return number1 * number2;
            }case DIV: {
                return number1 / number2;
            }
        }
        return 0;
    }


    public boolean play(){
        Scanner skan = new Scanner(System.in);
        int expectedResult = getResult();
        switch (randomOperator){
            case ADD: {
                System.out.println(number1 + " + " + number2 + " = ");
                break;
            }
            case SUB: {
                System.out.println(number1 + " - " + number2 + " = ");
                break;
            }
            case MUL: {
                System.out.println(number1 + " * " + number2 + " = ");
                break;
            }
            case DIV: {
                System.out.println(number1 + " / " + number2 + " = ");
                break;
            }
        }
        int actualResult = skan.nextInt();
        if(actualResult == expectedResult){
            return true;
        }
        else return false;
    }
}
