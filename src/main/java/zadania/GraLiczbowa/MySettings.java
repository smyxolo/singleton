package zadania.GraLiczbowa;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;
//
@Getter
@Setter

public class MySettings {
    private int number1Range;
    private int number2Range;
    private List<Operators> usedOperators = new ArrayList<>();
    private int roundLimit;

    private MySettings(){};

    private static final MySettings INSTANCE = new MySettings();

    public static MySettings getInstance(){
        return INSTANCE;
    }

//    public int getNumber1Range() {
//        return number1Range;
//    }
//
//    public void setNumber1Range(int number1Range) {
//        this.number1Range = number1Range;
//    }
//
//    public int getNumber2Range() {
//        return number2Range;
//    }
//
//    public void setNumber2Range(int number2Range) {
//        this.number2Range = number2Range;
//    }
//
//    public List<Operators> getUsedOperators() {
//        return usedOperators;
//    }
//
//    public int getRoundLimit() {
//        return roundLimit;
//    }
//
//    public void setRoundLimit(int roundLimit) {
//        this.roundLimit = roundLimit;
//    }



}
